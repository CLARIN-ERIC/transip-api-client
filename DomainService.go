/*
Reference:
	https://api.transip.nl/docs/transip.nl/class-Transip_DomainService.html
 */
package main

import (
	"fmt"
	"encoding/xml"
)

const domainServiceName = "DomainService"


type domainServiceGetInfo struct {
	XMLName xml.Name `xml:"ns1:getInfo"`
	DomainName domainName `xml:","`
}

type domainName struct {
	XMLName xml.Name `xml:"domainName"`
	Type string `xml:"xsi:type,attr"`
	TLD string `xml:",chardata"`
}

type DomainService struct {
	apiPtr *APISettings
}

func NewDomainService(apiPtr *APISettings) (*DomainService) {
	return &DomainService{
		apiPtr: apiPtr,
	}
}


func (s *DomainService) GetInfo(tld string) {
	soapBody := &domainServiceGetInfo{DomainName:domainName{Type:"xsd:string",TLD:tld}}
	response, err := s.apiPtr.soapRequest(domainServiceName, "getInfo", soapBody, "0="+tld)
	if err != nil {
		fmt.Printf("Failed get domain info. Error: %v\n", err)
		return
	}

	var domainInfo InfoResponse
	err = xml.Unmarshal(response, &domainInfo)
	if err != nil {
		fmt.Printf("Failed to unmarshal SOAP response. Error: %v\n", err)
	} else {
		fmt.Printf("Domain Info:\n")
		fmt.Printf("\tName               : %s\n", domainInfo.Return.Name)
		fmt.Printf("\tRegistration date  : %s\n", domainInfo.Return.RegistrationDate)
		fmt.Printf("\tRenewal date       : %s\n", domainInfo.Return.RenewalDate)
		fmt.Printf("\tAuthorization code : %s\n", domainInfo.Return.AuthCode)
		fmt.Printf("\tLocked             : %v\n", domainInfo.Return.IsLocked)
		fmt.Printf("\t#Nameservers       : %d\n", len(domainInfo.Return.Nameservers.List))

	}

}


type InfoResponse struct {
	XMLName xml.Name `xml:"getInfoResponse"`
	Return InfoResponseReturn
}

type InfoResponseReturn struct {
	XMLName xml.Name `xml:"return"`
	AuthCode string `xml:"authCode,omitempty"`
	Name string `xml:"name,omitempty"`
	RegistrationDate string `xml:"registrationDate,omitempty"`
	RenewalDate string `xml:"renewalDate,omitempty"`
	IsLocked bool `xml:"isLocked,omitempty"`
	Nameservers NameServerList `xml:"nameservers,omitempty"`
	ContactList ContactList `xml:"contacts,omitempty"`
}

type NameServerList struct {
	XMLName xml.Name `xml:"nameservers"`
	List []NameServerListItem `xml:"item"`
}

type NameServerListItem struct {
	XMLName xml.Name `xml:"item"`
	Hostname string `xml:"hostname,omitempty"`
	IpV4 string `xml:"ipv4,omitempty"`
	IpV6 string `xml:"ipv6,omitempty"`
}

type ContactList struct {
	XMLName xml.Name `xml:"contacts"`
	List []ContactListItem `xml:"item"`
}

type ContactListItem struct {
	XMLName xml.Name `xml:"item"`
	Type string `xml:"type,omitempty"`
	FirstName string `xml:"firstName,omitempty"`
	MiddleName string `xml:"middleName,omitempty"`
	LastName string `xml:"lastName,omitempty"`
	CompanyName string `xml:"companyName,omitempty"`
	ComponayKvk string `xml:"companyKvk,omitempty"`
	CompanyType string `xml:"companyType,omitempty"`
	Street string `xml:"street,omitempty"`
	Number string `xml:"number,omitempty"`
	PostalCode string `xml:"postalCode,omitempty"`
	City string `xml:"city,omitempty"`
	PhoneNumber string `xml:"phoneNumber,omitempty"`
	FaxNumber string `xml:"faxNumber,omitempty"`
	Email string `xml:"email,omitempty"`
	Country string `xml:"country,omitempty"`
}
/*
<ns1:getInfoResponse>
	<return xsi:type="ns1:Domain">
		<name xsi:type="xsd:string">cryptobase.nl</name>
		<authCode xsi:type="xsd:string">C56rCKMNR2xU</authCode>
		<registrationDate xsi:type="xsd:string">2014-11-06</registrationDate>
		<renewalDate xsi:type="xsd:string">2017-11-06</renewalDate>
		<isLocked xsi:type="xsd:boolean">false</isLocked>


		<nameservers SOAP-ENC:arrayType="ns1:Nameserver[3]" xsi:type="ns1:ArrayOfNameserver">
			<item xsi:type="ns1:Nameserver">
				<hostname xsi:type="xsd:string">ns0.transip.nl</hostname>
				<ipv4 xsi:type="xsd:string"></ipv4>
				<ipv6 xsi:type="xsd:string"></ipv6>
			</item>
			...
		</nameservers>
		<contacts SOAP-ENC:arrayType="ns1:WhoisContact[3]" xsi:type="ns1:ArrayOfWhoisContact">
			<item xsi:type="ns1:WhoisContact">
				<type xsi:type="xsd:string">registrant</type>
				<firstName xsi:type="xsd:string">W</firstName>
				<middleName xsi:type="xsd:string"></middleName>
				<lastName xsi:type="xsd:string">Elbers</lastName>
				<companyName xsi:type="xsd:string"></companyName>
				<companyKvk xsi:type="xsd:string"></companyKvk>
				<companyType xsi:type="xsd:string"></companyType>
				<street xsi:type="xsd:string">Past van Soevershemstr</street>
				<number xsi:type="xsd:string">5</number>
				<postalCode xsi:type="xsd:string">6525 SV</postalCode>
				<city xsi:type="xsd:string">NIJMEGEN</city>
				<phoneNumber xsi:type="xsd:string">+31 642138334</phoneNumber>
				<faxNumber xsi:type="xsd:string"></faxNumber>
				<email xsi:type="xsd:string">info@willemelbers.nl</email>
				<country xsi:type="xsd:string">nl</country>
			</item>
			...
		</contacts>
		<dnsEntries SOAP-ENC:arrayType="ns1:DnsEntry[13]" xsi:type="ns1:ArrayOfDnsEntry">
			<item xsi:type="ns1:DnsEntry">
				<name xsi:type="xsd:string">@</name>
				<expire xsi:type="xsd:int">86400</expire>
				<type xsi:type="xsd:string">A</type>
				<content xsi:type="xsd:string">37.97.254.27</content>
			</item>
			...
		</dnsEntries>
		<branding xsi:type="ns1:DomainBranding">
			<companyName xsi:type="xsd:string">TransIP BV</companyName>
			<supportEmail xsi:type="xsd:string">support@transip.nl</supportEmail>
			<companyUrl xsi:type="xsd:string">http://www.transip.nl</companyUrl>
			<termsOfUsageUrl xsi:type="xsd:string"></termsOfUsageUrl>
			<bannerLine1 xsi:type="xsd:string">TransIP BV</bannerLine1>
			<bannerLine2 xsi:type="xsd:string">Real-time domeinregistratie en -beheer vanaf 4.99 Euro!</bannerLine2>
			<bannerLine3 xsi:type="xsd:string">http://www.transip.nl/products/domain/</bannerLine3>
		</branding>

	</return>
</ns1:getInfoResponse>
 */