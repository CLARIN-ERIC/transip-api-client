/**
Inspired by https://github.com/xor-gate/go-transip
 */
package main

import (
	"fmt"
	"os"
	//"strings"
	"github.com/spf13/cobra"
)

func getHaipService(username, keyfile string) (*HaipService) {
	apiPtr := NewTransipApiClient(username, keyfile)
	return NewHaipService(apiPtr)
}

func getHaips(username, keyfile string){
	srvPtr := getHaipService(username, keyfile)
	haips, err := srvPtr.GetHaips()
	if err != nil {
		fmt.Printf("Failed to get haips info. Error: %v\n", err)
	} else {
		fmt.Printf("Haips (%d): \n", len(haips))
		for _, haip := range haips {
			haip.PrintWithPrefix("\t")
		}
	}
}

func getHaip(username, keyfile, haipName string) {
	srvPtr := getHaipService(username, keyfile)
	haip, err := srvPtr.GetHaip(haipName)
	if err != nil {
		fmt.Printf("Failed to get haip info. Error: %v\n", err)
	} else {
		fmt.Printf("Haip: \n")
		haip.PrintWithPrefix("\t")
	}
}

func changeHaip(username, keyfile, haipName, vpsName string) {
	srvPtr := getHaipService(username, keyfile)
	status, err := srvPtr.ChangeHaipVps(haipName, vpsName)
	if err != nil {
		fmt.Printf("Failed to change haip vps. Error: %v\n", err)
	} else {
		fmt.Printf("Change haip (%s) to vps %s: %t\n", haipName, vpsName, status)
	}
}

func main() {
	var username string
	var keyfile string
	var haipName string
	var vpsName string
	var shortOutput bool

	var cmdHaip = &cobra.Command{
		Use:   "haip",
		Short: "High Available IP Service",
		Long: `Transip API High Available IP (HAIP) client.`,
	}

	var cmdHaipCheck = &cobra.Command{
		Use:   "check",
		Short: "Check if haip is pointing to a specific VPS",
		Long: `Check if haip is pointing to a specific VP`,
		PreRun: func(cmd *cobra.Command, args []string) {
			error := false
			if username == "" {
				fmt.Printf("Username is required\n")
				error = true
			}
			if keyfile == "" {
				fmt.Printf("Private key file is required\n")
				error = true
			}

			if error {
				cmd.Help()
				os.Exit(1)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			srvPtr := getHaipService(username, keyfile)
			status, err := srvPtr.CheckHaipVps(haipName, vpsName)
			if err != nil {
				if shortOutput {
					fmt.Printf("false\n")
				} else {
					fmt.Printf("Failed to check haip vps. Error: %v\n", err)
				}
				os.Exit(1)
			} else {
				if status {
					if shortOutput {
						fmt.Printf("true\n")
					} else {
						fmt.Printf("Haip %s is pointing to vps %s, status = %t\n", haipName, vpsName, status)
					}
					os.Exit(0)
				} else {
					if shortOutput {
						fmt.Printf("false\n")
					} else {
						fmt.Printf("Haip %s is NOT pointing to vps %s, status = %t\n", haipName, vpsName, status)
					}
					os.Exit(1)
				}
			}
		},
	}
	cmdHaipCheck.Flags().StringVarP(&haipName, "name", "n", "", "Name of the high available IP")
	cmdHaipCheck.Flags().StringVarP(&vpsName, "vps", "V", "", "Name of the VPS")


	var cmdHaipList = &cobra.Command{
		Use:   "list",
		Short: "List haips",
		Long: `List all high available IPs`,
		PreRun: func(cmd *cobra.Command, args []string) {
			error := false
			if username == "" {
				fmt.Printf("Username is required\n")
				error = true
			}
			if keyfile == "" {
				fmt.Printf("Private key file is required\n")
				error = true
			}

			if error {
				cmd.Help()
				os.Exit(1)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			getHaips(username, keyfile)
		},
	}

	var cmdHaipGet = &cobra.Command{
		Use:   "get",
		Short: "Get haip details",
		Long: `Get info for the specified high available IP`,
		PreRun: func(cmd *cobra.Command, args []string) {
			error := false
			if username == "" {
				fmt.Printf("Username is required\n")
				error = true
			}
			if keyfile == "" {
				fmt.Printf("Private key file is required\n")
				error = true
			}
			if haipName == "" {
				fmt.Printf("Haip name is required\n")
				error = true
			}

			if error {
				cmd.Help()
				os.Exit(1)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			getHaip(username, keyfile, haipName)
		},
	}
	cmdHaipGet.Flags().StringVarP(&haipName, "name", "n", "", "Name of the high available IP")

	var cmdHaipChange = &cobra.Command{
		Use:   "change",
		Short: "Change the vps for haip",
		Long: `Change the vps associated with the specified haip`,
		PreRun: func(cmd *cobra.Command, args []string) {
			error := false
			if username == "" {
				fmt.Printf("Username is required\n")
				error = true
			}
			if keyfile == "" {
				fmt.Printf("Private key file is required\n")
				error = true
			}
			if haipName == "" {
				fmt.Printf("Haip name is required\n")
				error = true
			}
			if vpsName == "" {
				fmt.Printf("VPS name is required\n")
				error = true
			}

			if error {
				cmd.Help()
				os.Exit(1)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			changeHaip(username, keyfile, haipName, vpsName)
		},
	}
	cmdHaipChange.Flags().StringVarP(&haipName, "name", "n", "", "Name of the high available IP")
	cmdHaipChange.Flags().StringVarP(&vpsName, "vps", "V", "", "Name of the VPS")

	var rootCmd = &cobra.Command{
		Use:   "transip [command]",
		Short: "Interact with the transip API",
		Long: `Transip API client.`,}
	rootCmd.PersistentFlags().StringVarP(&username, "username", "u", "clarineu", "Transip account username")
	rootCmd.PersistentFlags().StringVarP(&keyfile, "keyfile", "k", "private_key.pem", "Transip api privaye key file (pem)")
	rootCmd.PersistentFlags().BoolVarP(&shortOutput, "short", "s", false, "Short output. This is tailored to automated processing and will swallow error messages.")

	rootCmd.AddCommand(cmdHaip)
	cmdHaip.AddCommand(cmdHaipList, cmdHaipGet, cmdHaipChange, cmdHaipCheck)
	rootCmd.Execute()
}