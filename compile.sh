#!/bin/bash
export GOPATH=/Users/wilelb/Code/work/clarin/git/infrastructure2

echo "Compilig for linux"
export GOOS=linux
export GOARCH=amd64
go build -o transip -ldflags="-s -w" *.go
tar -pczvf transip_linux.tar.gz transip
md5 transip_linux.tar.gz >> transip_linux.tar.gz.md5
rm transip

echo "Compiling for osx"
export GOOS=darwin
export GOARCH=amd64
go build -o transip -ldflags="-s -w" *.go
tar -pczvf transip_osx.tar.gz transip
md5 transip_osx.tar.gz >> transip_osx.tar.gz.md5
rm transip
