package main

import (
	"fmt"
	"time"
	"strings"
	"errors"
	"io/ioutil"
	"encoding/pem"
	"encoding/base64"
	"encoding/xml"
	"crypto"
	"crypto/x509"
	"crypto/sha512"
	"crypto/rsa"
	"crypto/rand"
	"net/url"
	"net/http"
	"net/http/httputil"
)

type APIMode string

const (
	APIModeReadOnly APIMode = "readonly"
	APIModeReadWrite APIMode = "readwrite"
)

const APISettingsDefaultClientVersion = "5.4"
const APISettingsDefaultEndpoint = "api.transip.nl"
const APISettingsDefaultMode = APIModeReadOnly

type APISettings struct {
	ClientVersion string
	Mode APIMode
	Endpoint string
	Login string
	PrivateKey string
	privateKey interface{}
	Verbose bool
	VeryVerbose bool
}

func (a *APISettings) String() string {
	summary := `
APISettings:
	 Client version: %v
	 Endpoint: %v
	 Mode: %v
	 Login: %v
	 PrivateKey: %v`

	return fmt.Sprintf(summary, a.ClientVersion, a.Endpoint, a.Mode, a.Login, a.PrivateKey)
}

func NewTransipApiClient(login, keyfile string) *APISettings {
	settings := APISettings {
		ClientVersion: APISettingsDefaultClientVersion,
		Endpoint: APISettingsDefaultEndpoint,
		Mode: APISettingsDefaultMode,
		PrivateKey: keyfile, //"private_key.pem",
		Login: login,
		Verbose: false,
		VeryVerbose: false,
	}
	settings.LoadPrivateKey()
	return &settings
}

func (a *APISettings) LoadPrivateKey() error {
	c, err := ioutil.ReadFile(a.PrivateKey)
	if err != nil {
		return err
	}

	block, _ := pem.Decode(c)
	if block == nil {
		return nil
	}

	a.privateKey, _ = x509.ParsePKCS8PrivateKey(block.Bytes)

	return nil
}

// Input: URL encoded params string
// Output: Base64 signed string
func (a *APISettings) authSign(params string) (string, error) {
	if a.privateKey == nil {
		return "", errors.New("Private key is required")
	}
	digest := sha512.Sum512([]byte(params))
	sig, err := rsa.SignPKCS1v15(nil, a.privateKey.(*rsa.PrivateKey), crypto.SHA512, digest[:])
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(sig), nil
}

func (a *APISettings) generateNonce() string {
	b := make([]byte, 7)
	rand.Read(b)
	return fmt.Sprintf("%x.%d", b, time.Now().Unix())
}

func (a *APISettings) generateRequestCookie(timestamp int64, nonce, signature string) (string,error) {
	if a.Login == "" {
		return "", errors.New("Login is required")
	}
	return fmt.Sprintf("login=%s;mode=%s;timestamp=%d;nonce=%s;clientVersion=%s;signature=%s;",
		a.Login, a.Mode, timestamp, nonce, a.ClientVersion, url.QueryEscape(signature)), nil
}

func (a *APISettings) soapRequest(soapService, soapMethod string, soapBody interface{}, soapParams string) ([]byte, error) {
	var emptyResult []byte

	//Prepare SOAP request
	soapRequest := SOAPEnvelope{
			ENV: "http://schemas.xmlsoap.org/soap/envelope/",
			NS1: "http://www.transip.nl/soap",
			XSD: "http://www.w3.org/2001/XMLSchema",
			XSI: "http://www.w3.org/2001/XMLSchema-instance",
			Enc: "http://schemas.xmlsoap.org/soap/encoding/",
			EncodingStyle: "http://schemas.xmlsoap.org/soap/encoding/",
	}
	soapRequest.Body.Content = soapBody

	//Marshall soap request to string
	b, err := xml.Marshal(&soapRequest)
	if err != nil {
		return emptyResult, errors.New(fmt.Sprintf("Failed to marshall SOAP request. Error: %v\n", err))
	}

	body := xml.Header + string(b)

	if a.Verbose {
		fmt.Printf("Request: %s(%s)\n", soapMethod, soapParams)
	}
	if a.VeryVerbose {
		fmt.Printf("Soap body:\n%s\n\n", body)
	}

	reader := strings.NewReader(body)
	req, _ := http.NewRequest("POST", a.uriSoap(soapService), reader)

	timestamp := time.Now().Unix()
	nonce := a.generateNonce()

	params := ""
	if soapParams == "" {
		params = fmt.Sprintf("__method=" + soapMethod + "&__service=" + soapService + "&__hostname=" + a.Endpoint + "&__timestamp=%d&__nonce=%s", timestamp, nonce)
	} else {
		params = fmt.Sprintf("%s&__method="+soapMethod+"&__service="+soapService+"&__hostname="+a.Endpoint+"&__timestamp=%d&__nonce=%s", soapParams, timestamp, nonce)
	}

	signature, err := a.authSign(params)
	if err != nil {
		return emptyResult, errors.New(fmt.Sprintf("Failed to sign. Error: %v\n", err))
	}
	cookie, err := a.generateRequestCookie(timestamp, nonce, signature)
	if err != nil {
		return emptyResult, errors.New(fmt.Sprintf("Failed to generate cookie. Error: %v\n", err))
	}

	req.Header.Set("Cookie", cookie)
	req.Header.Set("Content-type", "text/xml; charset=utf-8")

	client := &http.Client{}
	resp, _ := client.Do(req)
	data, _ := httputil.DumpResponse(resp, true)

	var soapResponse SOAPResponseEnvelope
	err = xml.Unmarshal(data, &soapResponse)
	if err != nil {
		return emptyResult, errors.New(fmt.Sprintf("Failed to unmarshal SOAP response. Error: %v\n", err))
	} else {
		if soapResponse.Body.Fault != nil {
			return emptyResult, errors.New(fmt.Sprintf("Soap request failed. Fault code: %v, message: %s\n", soapResponse.Body.Fault.Code, string(soapResponse.Body.Content)))
		} else {
			return soapResponse.Body.Content, nil
		}
	}

	return emptyResult, errors.New("Unkown error")
}
/*
func (a *APISettings) uriWsdl(service string) string {
	return "https://" + a.Endpoint + "/wsdl/?service=" + service
}
*/
func (a *APISettings) uriSoap(service string) string {
	return "https://" + a.Endpoint + "/soap/?service=" + service
}