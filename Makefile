#!/bin/bash

_GOPATH="/Users/wilelb/Code/work/clarin/git/infrastructure2/golang"

all: linux osx

linux:
	echo "Compilig for linux"
	GOPATH=${_GOPATH} GOOS=linux GOARCH=amd64 go build -o transip -ldflags="-s -w" *.go
	tar -pczvf transip_linux.tar.gz transip
	md5 transip_linux.tar.gz >> transip_linux.tar.gz.md5
	rm transip

osx:
	echo "Compiling for osx"
	GOPATH=${_GOPATH} GOOS=darwin GOARCH=amd64 go build -o transip -ldflags="-s -w" *.go
	tar -pczvf transip_osx.tar.gz transip
	md5 transip_osx.tar.gz >> transip_osx.tar.gz.md5
	rm transip
