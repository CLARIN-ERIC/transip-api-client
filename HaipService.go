/*
Reference:
	https://api.transip.nl/docs/transip.nl/class-Transip_HaipService.html
 */
package main

import (
	"fmt"
	"errors"
	"encoding/xml"
)

const haipServiceName = "HaipService"

type haipServiceGetHaips struct {
	XMLName xml.Name `xml:"ns1:getHaips"`
}

type haipServiceGetHaip struct {
	XMLName xml.Name `xml:"ns1:getHaip"`
	HaipName soapHaipName `xml:","`
}

type haipServiceChangeHaipVps struct {
	XMLName xml.Name `xml:"ns1:changeHaipVps"`
	HaipName soapHaipName `xml:","`
	VpsName soapVpsName `xml:","`
}


type soapHaipName struct {
	XMLName xml.Name `xml:"haipName"`
	Type string `xml:"xsi:type,attr"`
	Name string `xml:",chardata"`
}

type soapVpsName struct {
	XMLName xml.Name `xml:"vpsName"`
	Type string `xml:"xsi:type,attr"`
	Name string `xml:",chardata"`
}

type HaipService struct {
	apiPtr *APISettings
}

type GetHaipsResponse struct {
	XMLName xml.Name `xml:"getHaipsResponse"`
	Return GetHaipsReturn  `xml:"return"`
}

type GetHaipsReturn struct {
	XMLName xml.Name `xml:"return"`
	Haips []HaipItem  `xml:"item"`
}

type HaipItem struct {
	XMLName xml.Name `xml:"item"`
	Name string `xml:"name"`
	Status string `xml:"status"`
	IsBlocked bool `xml:"isBlocked"`
	Ipv4Address string `xml:"ipv4Address"`
	Ipv6Address string `xml:"ipv6Address"`
	VpsName string `xml:"vpsName"`
	VpsIpv4Address string `xml:"vpsIpv4Address"`
	VpsIpv6Address string `xml:"vpsIpv6Address"`
}

type GetHaipResponse struct {
	XMLName xml.Name `xml:"getHaipResponse"`
	Return GetHaipReturn  `xml:"return"`
}

type GetHaipReturn struct {
	XMLName xml.Name `xml:"return"`
	Name string `xml:"name"`
	Status string `xml:"status"`
	IsBlocked bool `xml:"isBlocked"`
	Ipv4Address string `xml:"ipv4Address"`
	Ipv6Address string `xml:"ipv6Address"`
	VpsName string `xml:"vpsName"`
	VpsIpv4Address string `xml:"vpsIpv4Address"`
	VpsIpv6Address string `xml:"vpsIpv6Address"`
}

type ChangeHaipVpsResponse struct {
	XMLName xml.Name `xml:"changeHaipVpsResponse"`
}

type Haip struct {
	Name           string
	Status         string
	IsBlocked      bool
	Ipv4Address    string
	Ipv6Address    string
	VpsName        string
	VpsIpv4Address string
	VpsIpv6Address string
}

func (h *Haip) Print() {
	h.PrintWithPrefix("")

}

func (h *Haip) PrintWithPrefix(prefix string) {
	fmt.Printf("%sName          : %s\n", prefix, h.Name)
	fmt.Printf("%sStatus        : %s\n", prefix, h.Status)
	fmt.Printf("%sIsBlocked     : %t\n", prefix, h.IsBlocked)
	fmt.Printf("%sIpv4Address   : %s\n", prefix, h.Ipv4Address)
	fmt.Printf("%sIpv6Address   : %s\n", prefix, h.Ipv6Address)
	fmt.Printf("%sVpsName       : %s\n", prefix, h.VpsName)
	fmt.Printf("%sVpsIpv4Address: %s\n", prefix, h.VpsIpv4Address)
	fmt.Printf("%sVpsIpv6Address: %s\n", prefix, h.VpsIpv6Address)

}

func NewHaipService(apiPtr *APISettings) (*HaipService) {
	return &HaipService{
		apiPtr: apiPtr,
	}
}

func (s *HaipService) GetHaips() ([]Haip, error) {
	method := "getHaips"
	result := make([]Haip, 0, 0)
	s.apiPtr.Mode = APIModeReadOnly
	soapBody := &haipServiceGetHaips{}
	response, err := s.apiPtr.soapRequest(haipServiceName, method, soapBody, "")
	if err != nil {
		return result, errors.New(fmt.Sprintf("\nFailed to %s. Error: %v\n", method, err))
	} else {
		var getHaipsResponse GetHaipsResponse
		err = xml.Unmarshal(response, &getHaipsResponse)
		if err != nil {
			return result, errors.New(fmt.Sprintf("\nFailed to unmarshal %s response. Error: %v\n", method, err))
		} else {
			for _, returnHaip := range getHaipsResponse.Return.Haips {
				haip := Haip{
					Name: returnHaip.Name,
					Status: returnHaip.Status,
					IsBlocked: returnHaip.IsBlocked,
					Ipv4Address: returnHaip.Ipv4Address,
					Ipv6Address: returnHaip.Ipv6Address,
					VpsName: returnHaip.VpsName,
					VpsIpv4Address: returnHaip.VpsIpv4Address,
					VpsIpv6Address: returnHaip.VpsIpv6Address,
				}
				result = append(result, haip)
			}
			return result, nil
		}
	}
	return result, errors.New(fmt.Sprintf("Unkown error for method %s.", method))
}

func (s *HaipService) GetHaip(haipName string) (*Haip, error) {
	method := "getHaip"
	var result *Haip
	soapBody := &haipServiceGetHaip{HaipName: soapHaipName{Type: "xsd:string", Name: haipName}}
	s.apiPtr.Mode = APIModeReadOnly
	response, err := s.apiPtr.soapRequest(haipServiceName, method, soapBody, "0="+haipName)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("\nFailed to %s Error: %v\n", method, err))
	} else {
		var getHaipResponse GetHaipResponse
		err = xml.Unmarshal(response, &getHaipResponse)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("\nFailed to unmarshal %s response. Error: %v\n", method, err))
		} else {
			haip := Haip{
				Name: getHaipResponse.Return.Name,
				Status: getHaipResponse.Return.Status,
				IsBlocked: getHaipResponse.Return.IsBlocked,
				Ipv4Address: getHaipResponse.Return.Ipv4Address,
				Ipv6Address: getHaipResponse.Return.Ipv6Address,
				VpsName: getHaipResponse.Return.VpsName,
				VpsIpv4Address: getHaipResponse.Return.VpsIpv4Address,
				VpsIpv6Address: getHaipResponse.Return.VpsIpv6Address,
			}
			result = &haip
			return result, nil
		}
	}

	return result, errors.New(fmt.Sprintf("Unkown error for method %s.", method))
}

func (s *HaipService) ChangeHaipVps(haipName, vpsName string) (bool, error) {
	method := "changeHaipVps"
	soapBody := &haipServiceChangeHaipVps{
		HaipName: soapHaipName{Type: "xsd:string", Name: haipName},
		VpsName: soapVpsName{Type: "xsd: string", Name: vpsName},
	}
	s.apiPtr.Mode = APIModeReadWrite
	response, err := s.apiPtr.soapRequest(haipServiceName, method, soapBody, "0="+haipName+"&1="+vpsName)
	if err != nil {
		return false, errors.New(fmt.Sprintf("Failed to %s. Error: %v\n", method, err))
	} else {
		var changeHaipVpsResponse ChangeHaipVpsResponse
		err = xml.Unmarshal(response, &changeHaipVpsResponse)
		if err != nil {
			return false, errors.New(fmt.Sprintf("Failed to unmarshal %s response. Error: %v\n", method, err))
		} else {
			return true, nil
		}
	}
	return true, errors.New(fmt.Sprintf("Unkown error for method %s.", method))
}

func (s *HaipService) CheckHaipVps(haipName, vpsName string) (bool, error) {
	haipInfo, err := s.GetHaip(haipName)
	if err != nil {
		return false, err
	}

	result := haipInfo.VpsName == vpsName
	return result, nil
}